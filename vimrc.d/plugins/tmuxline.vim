let g:tmuxline_theme = 'powerline'
let g:airline#extensions#tmuxline#enabled = 0

let g:tmuxline_preset = 'full'

" Don't use the powerline symbols!
" let g:tmuxline_powerline_separators = 0

let g:tmuxline_separators = {
    \ 'left' : "",
    \ 'left_alt': "",
    \ 'right' : "",
    \ 'right_alt' : "",
    \ 'space' : ' '}

