" fixes endwise mapping conflict
" https://github.com/tpope/vim-endwise/issues/125#issuecomment-727534673
inoremap <silent><expr> <cr>
      \ pumvisible() ? coc#_select_confirm() :
      \ "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>\<c-r>=EndwiseDiscretionary()\<CR>"
