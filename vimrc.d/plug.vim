" load up vim plug
call plug#begin('~/.vim/plugged')

" plugins
Plug 'editorconfig/editorconfig-vim'                " use editorconfig files
Plug 'edkolev/promptline.vim'                       " pretty terminal status line
Plug 'edkolev/tmuxline.vim'                         " pretty tmux status line
Plug 'gabesoft/vim-ags'                             " silversearcher plugin
Plug 'jiangmiao/auto-pairs'                         " bracket pairing
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } } " fzf plugin
Plug 'junegunn/fzf.vim'                             " fuzzy search
Plug 'junegunn/vim-easy-align'                      " easy alignment
Plug 'mhinz/vim-signify'                            " show git changes on the size
Plug 'miyakogi/seiya.vim'                           " toggle opaque background
Plug 'octol/vim-cpp-enhanced-highlight'             " cpp highlighting
Plug 'scrooloose/nerdtree'                          " file explorer
Plug 'tobyS/vmustache'                              " mustache implementation in vimscript
Plug 'tpope/vim-commentary'                         " comment stuff out with gcc
Plug 'tpope/vim-endwise'                            " close if, for, do, etc blocks
Plug 'tpope/vim-eunuch'                             " rename, move, delete, etc files
Plug 'tpope/vim-fugitive'                           " best git plugin
Plug 'tpope/vim-haml'                               " get that ham
Plug 'tpope/vim-sensible'                           " sensible defaults
Plug 'tpope/vim-surround'                           " surround me -> 'surround me'
Plug 'vim-airline/vim-airline'                      " pretty vim status line
Plug 'vim-airline/vim-airline-themes'               " coz one ain't enough
Plug 'vim-scripts/matchit.zip'                      " % to next bracket
Plug 'honza/vim-snippets'                           " general snippets
Plug 'joaohkfaria/vim-jest-snippets'                " because lazy
Plug 'neoclide/coc.nvim', { 'branch': 'release' }   " LSP and more
Plug 'pangloss/vim-javascript'                      " js highlighting
Plug 'wellle/tmux-complete.vim'                     " complete words from a tmux panes
Plug 'ryanoasis/vim-devicons'                       " pretty icons everywhere

" themes
" Plug 'chriskempson/base16-vim' " pretty vim colorscheme
Plug 'base16-project/base16-vim'

" let vim plug know we are done
call plug#end()
