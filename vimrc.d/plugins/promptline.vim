" Use our lightline colors!
let g:promptline_theme = 'airline'

" We don't need much
let g:promptline_preset = {
      \ 'a':    [ promptline#slices#user() ],
      \ 'b':    [ promptline#slices#cwd() ],
      \ 'x':    [ promptline#slices#git_status() ],
      \ 'y':    [ promptline#slices#vcs_branch() ],
      \ 'warn': [ promptline#slices#last_exit_code(), promptline#slices#jobs() ],
      \ 'z':    [ '%T' ]
      \ }

