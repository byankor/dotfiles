Dotfiles
===

New dotfiles! \o/

## Setup

### Essentials
```shell
sudo apt install xsel curl git cmake build-essential python-dev tmux tmuxp clang silversearcher-ag -y
```
---

### Tilix and Zsh
```shell
sudo apt install tilix zsh -y
```

Set tilix and zsh as defaults
```shell
chsh -s $(which zsh)
sudo update-alternatives --config x-terminal-emulator

# in tilix, set terminal to run command on open
tmux -2
```
*Note that you need to logout or reboot for zsh default to take effect*

---

### Neovim
```shell
sudo apt install neovim python3 -y
```
---

### Fonts

```shell 
sudo apt install fonts-hack-ttf
```

#### Install Patched Hack Font

Install patched Nerd Font so icons work.

  1. Download latest [Hack Font][1]
  2. Unzip to `~/.fonts`
  3. Run `fc-cach -fv`

[1]: https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/Hack.zip

---

### Now we can install our dotfiles

```shell
# clone this repo to `~/dotfiles`
git clone https://gitlab.com/byankor/dotfiles

# go into director and run install
cd dotfiles
./install
```
---

## TODO

  1. Setup SSH keys
  2. ~~Tmux lower left separtor should maybe be just | instead of <~~
  3. Make golang and nodejs scripts more robust
  4. Create projects folder in install script
  5. NodeJS is required on first install because coc
  6. Attempt COCInstall during install so we don't get it on first opening of nvim 
